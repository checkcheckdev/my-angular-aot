import { Room } from './Room';
import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import 'meteor-client';

@Component({
    selector: 'cc-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {

    count = 1;
    title = 'AppComponent';
    roomTitle = 'RoomComponent';
    clickedRoom = '';

    rooms: Array<Room>;

    constructor(private changeDetectorRef: ChangeDetectorRef) {
        this.count = 1;
        this.rooms = new Array<Room>();
        this.rooms.push(new Room('Room 111'));
        this.rooms.push(new Room('Room 222'));
        this.rooms.push(new Room('Room 333'));
    }

    changeProperties(): void {
        this.rooms.forEach(r => r.Name = 'Room Name ' + this.count);
        this.roomTitle = 'RoomComponent' + this.count;
        this.count++;
    }

    changeObjects(): void {
        this.rooms = new Array<Room>();
        this.rooms.push(new Room('Room 1' + this.count));
        this.rooms.push(new Room('Room 2' + this.count));
        this.rooms.push(new Room('Room 3' + this.count));
        this.rooms.push(new Room('Room 4' + this.count));
        this.count++;
    }

    onRoomClick(roomName: string): void {
        this.clickedRoom = roomName;
        this.changeProperties();
    }
}
