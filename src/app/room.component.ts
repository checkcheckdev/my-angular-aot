import {
    Component, Input, ChangeDetectionStrategy, OnChanges, SimpleChanges, Output, EventEmitter, ChangeDetectorRef
} from '@angular/core';
import { Room } from './Room';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'cc-room',
    templateUrl: './room.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoomComponent implements OnChanges {

    @Input() room: Room;
    // @Input() roomTitle: string;
    roomNumber = 0;

    @Output() roomClick: EventEmitter<string> = new EventEmitter<string>();

    constructor(private changeDetectorRef: ChangeDetectorRef) {
        setInterval(() => {
            this.roomNumber++;
            // this.changeDetectorRef.markForCheck();
        }, 1000);
    }

    ngOnChanges(changes: SimpleChanges): void {
        console.log('RoomComponent ngOnChanges');
        if (changes['room']) {
            console.log('Change: ' + changes['room'].currentValue.Name);
        }
    }

    onClick(): void {
        this.roomNumber++;
        this.roomClick.emit(this.room.Name);

        Meteor.call('testCuong');
    }
}
