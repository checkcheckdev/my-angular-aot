import { MyAotAppPage } from './app.po';

describe('my-aot-app App', () => {
  let page: MyAotAppPage;

  beforeEach(() => {
    page = new MyAotAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
